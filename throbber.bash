function throbber()
{
  local rs="${1-\n}"
  gawk '
    BEGIN {RS="'"$rs"'"; ORS=RS; s="/-\\|"}
    {print $0}
    NR%100==0 {printf(substr(s,(NR/100)%4+1,1)"\b") > "/dev/stderr"}
    NR%1000==0 {printf(".") > "/dev/stderr"}
    END{print "" > "/dev/stderr"}
  '
}
